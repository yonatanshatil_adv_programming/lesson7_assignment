﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HW20
{
    public partial class Form1 : Form
    {
        int currCard;
        string lassPresed;
        int yourScore = 0;
        int oppScore = 0;
        bool is_pressed = false;
        NetworkStream clientStream;
        TcpClient client = new TcpClient();
        Dictionary<string, Image> cards = new Dictionary<string, Image>();
        public Form1()
        {
            InitializeComponent();
            setCards();
            Thread listen = new Thread(function);
            listen.Start();
            button1.Enabled = false;
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            MessageBox.Show(label1.Text + "\n" + label2.Text);
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void gnarateCards()
        {
            Point nextLocation = new Point(12,222);
            for (int i = 0; i < 10; i++)
            {
                // create the image object itself
                System.Windows.Forms.PictureBox currentPic = new PictureBox();
                currentPic.Name = "picDynamic" + i; // in our case, this is the only property that changes between the different images
                currentPic.Image = global::HW20.Properties.Resources.card_back_red;
                currentPic.Location = nextLocation;
                currentPic.Size = new System.Drawing.Size(blueCard.Width, blueCard.Height);
                currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                // assign an event to it
                currentPic.Click += delegate (object sender1, EventArgs e1)
                {
                    blueCard.Image = global::HW20.Properties.Resources.card_back_blue;
                    string[] typs = { "H", "C", "S", "D" };
                    Random r1 = new Random();
                    string card="";
                    int num = r1.Next(1, 13);
                    currCard = num;
                    string strNum;
                    if (num < 10)
                    {
                        strNum = "0" + num.ToString();
                    }
                    else
                    {
                        strNum = num.ToString();
                    }
                    int type = r1.Next(0, 3);
                    card += "1"  + strNum + typs[type];
                    byte[] buffer = new ASCIIEncoding().GetBytes(card);
                    clientStream.Write(buffer, 0, 4);
                    clientStream.Flush();
                    is_pressed = true;
                    lassPresed = this.Name;
                    ((PictureBox)sender1).Image = cards[card];
                };
                // add the picture object to the form (otherwise it won't be seen)
                this.Controls.Add(currentPic);

                // calculate the location point for the next card to be drawn/added
                nextLocation.X += currentPic.Size.Width + 10;
            }
        }
        private void function()
        {
            

            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);

            client.Connect(serverEndPoint);

            clientStream = client.GetStream();
            while(true)
            {
                byte[] bufferIn = new byte[4];
                int bytesRead = clientStream.Read(bufferIn, 0, 4);
                string input = new ASCIIEncoding().GetString(bufferIn);
                if(input=="0000")//starts the game
                {
                    Invoke((MethodInvoker)delegate { gnarateCards(); button1.Enabled = true; });
                }
                else if(input=="2000")
                {
                    MessageBox.Show(label1.Text + "\n" + label2.Text);
                    Application.Exit();
                }
                else
                {
                    if (is_pressed)
                    {
                        Invoke((MethodInvoker)delegate {
                            blueCard.Image = cards[input];
                            int oppCard = int.Parse(input.Substring(1, 2));
                            if (oppCard > currCard)
                            {
                                yourScore -= 1;
                                oppScore += 1;
                            }
                            else if (oppCard < currCard)
                            {
                                yourScore += 1;
                                oppScore -= 1;
                            }
                            label1.Text = "your score: " + yourScore.ToString();
                            label2.Text = "opponents score: " + oppScore.ToString();
                            is_pressed = false;
                        });
                    }
                    
                }
            }
        }
        private void setCards()
        {
            string strNum;
            string[] typs = { "H", "C", "S", "D" };
            for (int i = 0; i < 52; i++)
            {
                if ((i % 13) + 1 < 10)
                    strNum = "0" + ((i % 13) + 1).ToString();
                else
                    strNum = ((i % 13) + 1).ToString();
                int type = i / 13;
                string cardCode = "1"  + strNum + typs[type];
                string card = "_" + ((i % 13) + 1).ToString() + "_of_";
            switch (type)
                {
                    case 0:
                        card += "hearts";
                        break;
                    case 1:
                        card += "clubs";
                        break;
                    case 2:
                        card += "spades";
                        break;
                    case 3:
                        card += "diamonds";
                        break;
                }
                Image newImage=(Image)global::HW20.Properties.Resources.ResourceManager.GetObject(card);
                cards.Add(cardCode,newImage);
            }

                
        }
        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(label1.Text+"\n"+label2.Text);
            Application.Exit();
        }

        private void Form1_Load_1(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
